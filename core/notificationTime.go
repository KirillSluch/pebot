package core

import (
	"go.mongodb.org/mongo-driver/bson/primitive"
)

type Notification struct {
	ID             primitive.ObjectID `bson:"_id"`
	SubscriberTgID int64              `bson:"subscriber_tg_id"`
	Hour           int8               `bson:"hour"`
	Minute         int8               `bson:"minute"`
	Message        string             `bson:"message,omitempty"`
	IsActive       bool               `bson:"isActive"`
	IsRepeatable   bool               `bson:"isRepeatable"`
}
