package core

import "go.mongodb.org/mongo-driver/bson/primitive"

type Subscriber struct {
	ID   primitive.ObjectID `bson:"_id,omitempty"`
	TgID int64              `bson:"tg_id,omitempty"`
}
