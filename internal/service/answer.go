package service

import (
	"awesomeProject/core"
	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api"
	"log"
	"strings"
)

var keyboard = tgbotapi.NewReplyKeyboard(
	tgbotapi.NewKeyboardButtonRow(
		tgbotapi.NewKeyboardButton("/set_notification"),
		tgbotapi.NewKeyboardButton("/set_notification"),
		tgbotapi.NewKeyboardButton("/deactivate_notification"),
	),
)

type MessageHandler interface {
}

type SubscriberService interface {
	SetNotification(notification core.Notification) error
	RemoveNotification(notification core.Notification) error
}

type Handler interface {
	GetHandlingCommand() string
	HandleCommand(update tgbotapi.Update) (string, error)
}

type AnswerService struct {
	subscriberService SubscriberService
	handlers          []Handler
}

func NewAnswerService() *AnswerService {
	return &AnswerService{}
}

func (service *AnswerService) RegistryHandler(handler Handler) {
	service.handlers = append(service.handlers, handler)
}

func (service *AnswerService) Generate(update tgbotapi.Update) tgbotapi.MessageConfig {

	messageText := strings.Split(update.Message.Text, " ")
	var answerText string
	chatId := update.Message.Chat.ID

	log.Printf("[%s] %s", update.Message.From.UserName, messageText)

	for _, handler := range service.handlers {
		if handler.GetHandlingCommand() == messageText[0] {
			answerText, _ = handler.HandleCommand(update)
		}
	}

	if answerText == "" {
		answerText = "Команда не распознана"
	}
	return tgbotapi.NewMessage(chatId, answerText)
}
