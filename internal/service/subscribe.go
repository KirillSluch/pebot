package service

import (
	"awesomeProject/core"
	"context"
	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api"
	"log"
	"time"
)

type SubscriberRepository interface {
	GetAll(ctx context.Context) ([]*core.Subscriber, error)
	GetByTgId(ctx context.Context, tgId int64) (*core.Subscriber, error)
	Save(ctx context.Context, user *core.Subscriber) (*core.Subscriber, error)
	Delete(ctx context.Context, user *core.Subscriber) error
}

type NotificationRepository interface {
	GetCurrents(ctx context.Context, hour int8, minute int8) ([]*core.Notification, error)
	Save(ctx context.Context, notification *core.Notification) (*core.Notification, error)
	Delete(ctx context.Context, notification *core.Notification) error
	UpdateByTime(ctx context.Context, hour int8, minute int8, value bool) error
	DeleteNonRepeatable(ctx context.Context, hour int, minute int) error
}

type SubscribeService struct {
	subscriberRepository   SubscriberRepository
	notificationRepository NotificationRepository
}

const NotificationTimer = 60

func NewSubscribeService(subscriberRepository SubscriberRepository, notificationRepository NotificationRepository) *SubscribeService {
	return &SubscribeService{subscriberRepository: subscriberRepository, notificationRepository: notificationRepository}
}

func (service *SubscribeService) SetNotification(notification core.Notification) error {
	_, err := service.notificationRepository.Save(context.Background(), &notification)
	if err != nil {
		return err
	}
	return nil
}

func (service *SubscribeService) RemoveNotification(notification core.Notification) error {
	err := service.notificationRepository.Delete(context.Background(), &notification)
	if err != nil {
		return err
	}
	return nil
}

func (service *SubscribeService) InitSubscribers(bot *tgbotapi.BotAPI) {
	for {
		currentTime := time.Now()
		hour := int8(currentTime.Hour())
		minute := int8(currentTime.Minute())

		notifications := service.currentNotification(hour, minute)
		service.turnNotificationOff(hour, minute)
		service.turnNotificationOn(calcPreviousTime(hour, minute))

		for _, notification := range notifications {
			send(notification, bot)
		}

		service.deleteNotUsed(hour, minute)

		time.Sleep(NotificationTimer * time.Second)
	}
}

func send(notification *core.Notification, bot *tgbotapi.BotAPI) {
	tgId := notification.SubscriberTgID
	message := notification.Message
	bot.Send(tgbotapi.NewMessage(tgId, message))
}

func (service *SubscribeService) turnNotificationOff(hour int8, minute int8) {
	err := service.notificationRepository.UpdateByTime(context.Background(), hour, minute, false)
	if err != nil {
		log.Print(err.Error())
	}
}

func (service *SubscribeService) turnNotificationOn(hour int8, minute int8) {
	err := service.notificationRepository.UpdateByTime(context.Background(), hour, minute, true)
	if err != nil {
		log.Print(err.Error())
	}
}

func (service *SubscribeService) currentNotification(hour int8, minute int8) []*core.Notification {
	notifications, err := service.notificationRepository.GetCurrents(context.Background(), hour, minute)
	if err != nil {
		log.Print(err.Error())
	}
	return notifications
}

func calcPreviousTime(hour int8, minute int8) (int8, int8) {
	if hour == 0 && minute == 0 {
		return 23, 59
	} else if minute == 0 {
		return hour - 1, 59
	}
	return hour, minute - 1
}

func (service *SubscribeService) deleteNotUsed(hour int8, minute int8) {
	err := service.notificationRepository.DeleteNonRepeatable(context.Background(), int(hour), int(minute))
	if err != nil {
		log.Print(err.Error())
	}
}
