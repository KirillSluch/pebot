package handler

import (
	"awesomeProject/internal/service"
	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api"
)

type CommandHandler struct {
	command           string
	subscriberService *service.SubscribeService
	handleFunction    func(update tgbotapi.Update, subscriberService *service.SubscribeService) (string, error)
}

func NewCommandHandler(command string, subscriberService *service.SubscribeService,
	function func(update tgbotapi.Update, subscriberService *service.SubscribeService) (string, error)) *CommandHandler {
	return &CommandHandler{
		command:           command,
		subscriberService: subscriberService,
		handleFunction:    function,
	}
}

func (handler *CommandHandler) GetHandlingCommand() string {
	return handler.command
}

func (handler *CommandHandler) HandleCommand(update tgbotapi.Update) (string, error) {
	return handler.handleFunction(update, handler.subscriberService)
}
