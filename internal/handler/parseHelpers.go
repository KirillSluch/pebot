package handler

import (
	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api"
	"regexp"
	"strconv"
	"strings"
)

const CheckInputMessage = "Проверьте правильность ввода"

func ParseUpdate(update tgbotapi.Update) ([]string, int64) {
	messageText := strings.Split(update.Message.Text, " ")
	chatId := update.Message.Chat.ID
	return messageText, chatId
}

func TimeChecker(stringToCheck string) (matched bool, err error) {
	return regexp.MatchString(`\d?\d:\d\d`, stringToCheck)
}

func SeparateAndConvertTime(string string) (int, int, error) {
	ints := strings.Split(string, ":")

	h, err := strconv.Atoi(ints[0])
	if err != nil {
		return -1, -1, err
	}
	min, err := strconv.Atoi(ints[1])
	if err != nil {
		return -1, -1, err
	}

	return h, min, nil

}
