package handler

import (
	"awesomeProject/core"
	"awesomeProject/internal/service"
	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api"
	"log"
)

func SetHandle(update tgbotapi.Update, service *service.SubscribeService) (string, error) {

	messageText, chatId := ParseUpdate(update)

	var answer string
	matched, _ := TimeChecker(messageText[1])

	if matched {
		hours, minutes, err := SeparateAndConvertTime(messageText[1])

		if err != nil {
			return CheckInputMessage, err
		}

		err = (*service).SetNotification(core.Notification{
			SubscriberTgID: chatId,
			Hour:           int8(hours),
			Minute:         int8(minutes),
			Message:        messageText[2],
			IsActive:       true,
			IsRepeatable:   len(messageText) == 4,
		})

		if err != nil {
			log.Println("Ошибка базы")
			return "Что-то пошло не так (", err
		}
		answer = "Уведомление добавлено"
	} else {
		answer = CheckInputMessage
	}

	return answer, nil
}
