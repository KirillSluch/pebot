package handler

import (
	"awesomeProject/core"
	"awesomeProject/internal/service"
	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api"
	"log"
)

func RemoveHandle(update tgbotapi.Update, service *service.SubscribeService) (string, error) {

	messageText, chatId := ParseUpdate(update)

	var answer string
	matched, _ := TimeChecker(messageText[1])

	if matched {
		hours, minutes, err := SeparateAndConvertTime(messageText[1])

		if err != nil {
			return CheckInputMessage, err
		}
		err = (*service).RemoveNotification(core.Notification{
			SubscriberTgID: chatId,
			Hour:           int8(hours),
			Minute:         int8(minutes),
		})

		if err != nil {
			log.Println("Ошибка базы")
			return "Что-то пошло не так (", err
		}
		answer = "Уведомление удалено"
	} else {
		answer = CheckInputMessage
	}

	return answer, nil
}
