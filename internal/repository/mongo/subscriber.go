package mongo

import (
	"awesomeProject/core"
	"context"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
)

type SubscriberRepository struct {
	collection *mongo.Collection
}

func NewSubscriberRepository(collection *mongo.Collection) *SubscriberRepository {
	return &SubscriberRepository{collection: collection}
}

func (repository *SubscriberRepository) GetAll(ctx context.Context) ([]*core.Subscriber, error) {
	cursor, err := repository.collection.Find(ctx, bson.D{})

	if err != nil {
		return nil, err
	}

	subscribers := make([]*core.Subscriber, 0)

	err = cursor.All(ctx, subscribers)

	if err != nil {
		return nil, err
	}

	return subscribers, nil
}

func (repository *SubscriberRepository) GetByTgId(ctx context.Context, tgId int64) (*core.Subscriber, error) {
	filter := bson.M{"tg_id": tgId}

	subscriber := &core.Subscriber{}

	err := repository.collection.FindOne(ctx, filter).Decode(subscriber)

	if err != nil {
		return nil, err
	}

	return subscriber, nil
}

func (repository *SubscriberRepository) Save(ctx context.Context, subscriber *core.Subscriber) (*core.Subscriber, error) {
	result, err := repository.collection.InsertOne(ctx, subscriber)

	if err != nil {
		return nil, err
	}

	subscriber.ID = result.InsertedID.(primitive.ObjectID)

	return subscriber, nil
}

func (repository *SubscriberRepository) Delete(ctx context.Context, subscriber *core.Subscriber) error {
	filter := bson.M{"tg_id": subscriber.TgID}
	_, err := repository.collection.DeleteOne(ctx, filter)
	if err != nil {
		return err
	}
	return nil
}
