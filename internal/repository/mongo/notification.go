package mongo

import (
	"awesomeProject/core"
	"context"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
)

type NotificationRepository struct {
	collection *mongo.Collection
}

func NewNotificationRepository(collection *mongo.Collection) *NotificationRepository {
	return &NotificationRepository{collection: collection}
}

func (repository *NotificationRepository) GetCurrents(ctx context.Context,
	hour int8, minute int8) ([]*core.Notification, error) {
	filter := bson.M{"hour": hour, "minute": minute, "isActive": 1}
	cursor, err := repository.collection.Find(ctx, filter)

	if err != nil {
		return nil, err
	}

	notifications := make([]*core.Notification, 0)

	err = cursor.All(ctx, &notifications)

	if err != nil {
		return nil, err
	}
	return notifications, nil
}

func (repository *NotificationRepository) UpdateByTime(ctx context.Context, hour int8, minute int8, value bool) error {
	filter := bson.M{"hour": hour, "minute": minute}
	var update bson.D
	if value {
		update = bson.D{
			{"$set", bson.D{{"isActive", 1}}},
		}
	} else {
		update = bson.D{
			{"$set", bson.D{{"isActive", 0}}},
		}
	}
	_, err := repository.collection.UpdateMany(ctx, filter, update)

	if err != nil {
		return err
	}

	return nil
}

func (repository *NotificationRepository) Save(ctx context.Context, notification *core.Notification) (*core.Notification, error) {
	result, err := repository.collection.InsertOne(ctx, notification)

	if err != nil {
		return nil, err
	}

	notification.ID = result.InsertedID.(primitive.ObjectID)

	return notification, nil
}

func (repository *NotificationRepository) Delete(ctx context.Context, notification *core.Notification) error {
	filter := bson.M{
		"hour":             notification.Hour,
		"minute":           notification.Minute,
		"subscriber_tg_id": notification.SubscriberTgID,
	}
	_, err := repository.collection.DeleteMany(ctx, filter)
	if err != nil {
		return err
	}
	return nil
}

func (repository *NotificationRepository) DeleteNonRepeatable(ctx context.Context, hour int, minute int) error {
	filter := bson.M{
		"hour":         hour,
		"minute":       minute,
		"isRepeatable": false,
	}
	_, err := repository.collection.DeleteMany(ctx, filter)
	if err != nil {
		return err
	}
	return nil
}
