package bot

import (
	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api"
	"github.com/spf13/viper"
	"log"
)

type BotConfig struct {
	Token string
}

func InitBot() (*tgbotapi.BotAPI, tgbotapi.UpdateConfig) {
	config := &BotConfig{}
	err := viper.UnmarshalKey("bot", config)
	bot, err := tgbotapi.NewBotAPI(config.Token)
	if err != nil {
		log.Panic(err)
	}

	bot.Debug = true

	log.Printf("Authorized on account %s", bot.Self.UserName)

	u := tgbotapi.NewUpdate(0)
	u.Timeout = 60

	return bot, u
}
