package main

import (
	b "awesomeProject/internal/bot"
	"awesomeProject/internal/config"
	"awesomeProject/internal/handler"
	"awesomeProject/internal/repository/mongo"
	"awesomeProject/internal/service"
	"context"
	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api"
	"log"
	"time"
)

func main() {
	if err := config.SetupViper(); err != nil {
		log.Fatal(err.Error())
	}

	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)

	mongoDataBase, err := config.SetupMongoDB(ctx, cancel)

	if err != nil {
		log.Fatal(err.Error())
	}

	subscribeRepository := mongo.NewSubscriberRepository(mongoDataBase.Collection("subscribers"))
	notificationRepository := mongo.NewNotificationRepository(mongoDataBase.Collection("notification_time"))

	bot, u := b.InitBot()

	subscribeService := service.NewSubscribeService(subscribeRepository, notificationRepository)
	go subscribeService.InitSubscribers(bot)

	answerService := handlersRegister(subscribeService)

	updates, _ := bot.GetUpdatesChan(u)

	for update := range updates {
		if update.Message == nil { // ignore any non-Message Updates
			continue
		}

		var answer tgbotapi.MessageConfig

		answer = answerService.Generate(update)

		_, err := bot.Send(answer)
		if err != nil {
			log.Println(err.Error())
		}
	}
}

func handlersRegister(subscribeService *service.SubscribeService) *service.AnswerService {
	answerService := service.NewAnswerService()
	answerService.RegistryHandler(handler.NewCommandHandler("START", subscribeService, handler.StartHandle))
	answerService.RegistryHandler(handler.NewCommandHandler("SET", subscribeService, handler.SetHandle))
	answerService.RegistryHandler(handler.NewCommandHandler("DEL", subscribeService, handler.RemoveHandle))
	return answerService
}
